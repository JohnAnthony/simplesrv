package main

import (
	"fmt"
	"io"
	"net/http"
	"time"
)

func withLogging(out io.Writer, next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		go func(r *http.Request) {
			fmt.Fprintf(
				out,
				"[%s] \"GET %s\" \"%s\"\n",
				time.Now().Format(time.RFC3339),
				r.RequestURI,
				r.UserAgent(),
			)
		}(r)
		next.ServeHTTP(w, r)
	}
}
