# simplesrv: a command-line http file server

simplesrv is a simple, zero-configuration, zero-dependency command-line http
server in the vein of [http-server](https://www.npmjs.com/package/http-server).
Like that node-based package, it is powerful enough for production usage, but
it's simple and hackable enough to be used for testing, local development, and
learning.

# Installation

```bash
go get -u gitlab.com/JohnAnthony/simplesrv
````

# Usage

```bash
simplesrv <option1> <option2> <...> [path]
```

`[path]` has no default and must be provided. An example basic usage might be
`simplesrv .`, allowing you to now visit [http://localhost:8080](http://localhost:8080)
and view your files.

# Available options

Running `simplesrv -h` or `simplesrv` without options or path will display
usage information and available flags.

```
usage: simplesrv <option1> <option2> <...> [path]

options:
  -p <int>       Port to use [8080]
  -a <string>    Address to use [0.0.0.0]
  -s             Suppress log messages from output

  -S             Enable https
  -C <string>    Path to ssl cert file [./cert.pem].
  -K <string>    Path to ssl key file [./key.pem].

  -h             Print this list and exit
```

# TLS

Generating and using a self-signed certificate is easy

```bash
openssl genrsa -out key.pem 2048
openssl req -new -x509 -key key.pem -out cert.pem -days 3650
simplesrv -S /www
```

# Speed

Speed is unlikely to be a very large concern for simplesrv's use case, but
initial load testing appears to be extremely positive. On a directory with
1,000 files simplesrv's slowest response time was 8x faster than http-server's
fastest response time, and the mean response time was 200x faster.

```bash
~ mkdir /tmp/loadtest
~ touch /tmp/loadtest/file-{1..1000}

## Test pointed at `http-server /tmp/loadtest`
~ echo "GET http://localhost:8080" | vegeta attack -duration=10s | tee results.bin | vegeta report
Requests      [total, rate, throughput]         500, 50.10, 38.97
Duration      [total, attack, wait]             12.832s, 9.98s, 2.852s
Latencies     [min, mean, 50, 90, 95, 99, max]  95.023ms, 1.619s, 1.645s, 2.737s, 2.869s, 3.17s, 3.206s
Bytes In      [total, mean]                     133836500, 267673.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:500

## Test pointed at `simplesrv /tmp/loadtest`
~ echo "GET http://localhost:8080" | vegeta attack -duration=10s | tee results.bin | vegeta report
Requests      [total, rate, throughput]         500, 50.10, 50.07
Duration      [total, attack, wait]             9.985s, 9.98s, 5.36ms
Latencies     [min, mean, 50, 90, 95, 99, max]  3.421ms, 8.067ms, 8.818ms, 9.437ms, 9.784ms, 10.933ms, 12.508ms
Bytes In      [total, mean]                     15899500, 31799.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:500
Error Set:
```
