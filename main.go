package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

// Config represents configuration for an HTTP fileserver
type Config struct {
	name string

	address string
	port    int
	silent  bool
}

// StartServer starts an HTTP fileserver
func StartServer(out io.Writer, path string, conf *Config) {
	handler := http.FileServer(http.Dir(path))
	if !conf.silent {
		startupMessage(out, path, conf)
		handler = withLogging(out, handler)
	}

	srv := &http.Server{
		Addr:              fmt.Sprintf("%s:%d", conf.address, conf.port),
		ReadHeaderTimeout: 3 * time.Second,
	}

	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func startupMessage(out io.Writer, path string, conf *Config) {
	fmt.Printf("Starting up %s, serving %s\n", conf.name, path)
	fmt.Printf("Available on:\n")
	fmt.Printf("  http://%s:%d\n", conf.address, conf.port)
	fmt.Printf("Hit CTRL-C to stop the server\n")
}

func usage(out io.Writer, conf *Config) {
	fmt.Fprintf(out, `usage: %s <option1> <option2> <...> [path]

options:
	-p <int>       Port to use [8080]
	-a <string>    Address to use [0.0.0.0]
	-s             Suppress log messages from output

	-S             Enable https
	-C <string>    Path to ssl cert file [./cert.pem].
	-K <string>    Path to ssl key file [./key.pem].

	-h             Print this list and exit`, conf.name)
}

func main() {
	var help bool
	conf := Config{name: os.Args[0]}

	flag.StringVar(&conf.address, "a", "0.0.0.0", "Address to use")
	flag.IntVar(&conf.port, "p", 8080, "Port to use")
	flag.BoolVar(&conf.silent, "s", false, "Silent (no logging)")
	flag.BoolVar(&help, "h", false, "Display help/usage information")

	flag.Parse()
	if flag.NArg() != 1 || help {
		usage(os.Stdout, &conf)
		return
	}

	StartServer(os.Stdout, flag.Arg(0), &conf)
}
